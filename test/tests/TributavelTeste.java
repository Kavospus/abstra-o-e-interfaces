/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tests;

import abstratas.ContaPoupanca;
import interfaces.Tributavel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author andrebsguedes
 */
public class TributavelTeste {
    
    public TributavelTeste() {
    }
    ContaPoupanca p;
    @Before
    public void setUp() {
        p = new ContaPoupanca();
        p.setSaldo(10.0);
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    @Test
    public void testTributavel() {
        Tributavel t = p;
        t.calculaTributo();
        assertEquals(9.0,p.getSaldo(),0.01);
    }
}
