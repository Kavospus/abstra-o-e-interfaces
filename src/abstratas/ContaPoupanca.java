/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package abstratas;

import interfaces.Tributavel;

/**
 *
 * @author andrebsguedes
 */
public class ContaPoupanca extends Conta implements Tributavel{
    private double saldo;
    
    @Override
    public void atualiza(double taxaSelic) {
        this.setSaldo(this.getSaldo() + getSaldo() * taxaSelic); 
    }

    @Override
    public void calculaTributo() {
        this.setSaldo(this.getSaldo() - getSaldo() * 0.1);
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    
    
    
}
